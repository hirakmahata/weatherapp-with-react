import React, {useState} from "react"

const api = {
  key: "9d3b4c47835c56e9cbba518ebc03d7b2",
  base: "https://api.openweathermap.org/data/2.5/"
}

function App() {
  const [cityName, setCityName] = useState('')
  const [weather, setWeather] = useState({})

  const search = evt => {
    if(evt.key === "Enter"){
      fetch(`${api.base}weather?q=${cityName}&units=metric&appid=${api.key}`)
      .then(response => response.json())
      .then(result => {
        setWeather(result)
        if(result.cod ==="404"){
          document.getElementById("error-message").style="flex";
        }
      })
    }
  }

  const dateBuilder = (dt) => {
    let months =["January","February","March","April","May","June","July","August","September","october","november", "december"];
    let days =["sunday","monday","tuesday","wednesday", "thursday", "friday", "saturday"];

    let day = days[dt.getDay()];
    let date = dt.getDate();
    let month = months[dt.getMonth()];
    let year = dt.getFullYear();

    return `${day} ${date} ${month} ${year}`
   }

  return (
    <div className={(typeof weather.main != "undefined") ? ((weather.main.temp>=17)?'warm-app':'cold-app') : 'app'}>
      <main>
        <div className={(typeof weather.main != "undefined") ?((weather.main.temp<17)?'cold-heading':'heading'):'heading'}
        >Search for city and press "ENTER" to get weather details</div>
        <div className="search-box">
          <input 
          type ="text"
          className="search-bar" 
          placeholder="search for the city.."
          onChange={e => setCityName(e.target.value)}
          value={cityName}
          onKeyPress={search}
          />
        </div>
        {(typeof weather.main != "undefined") ? (
          <div>
              <div className="location-box">
            <div className="location">{weather.name}, {weather.sys.country}</div>
            <div className="date">{dateBuilder(new Date())}</div>
          </div>
          <div className="weather-box">
            <div className="temp">
              {Math.round(weather.main.temp)}°c
            </div>
            <div className="weather">
              {weather.weather[0].main}
            </div>
            <div className="min-max">
              <span className="min">min : {Math.round(weather.main.temp_min)}°c</span>
              <span className="max">max : {Math.round(weather.main.temp_max)}°c</span>
            </div>
          </div>
          </div>
        ) : (
          <div style={{display:"none"}} id="error-message">please enter a valid city</div>
        )}
      </main>
    </div>
  );
}

export default App;
